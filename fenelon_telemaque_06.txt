<<< sommaire  <<< page précédente  page suivante >>>

B  I  B  L  I  O  T  H  E  C  A    A  U  G  U  S  T  A  N  A

 

 

 

 

Fénelon
Les aventures de Télémaque

 
 

 

S i x i è m e  l i v r e
[]
Mentor précipite Télémaque à la mer,
en appercevant l'incendie de son vaisseau.
_________________________
Sommaire de l'édition dite de Versailles (1824)
Calypso, ravie d'admiration par le récit de Télémaque, conçoit
pour lui une violente passion, et met tout en oeuvre pour
exciter en lui le même sentiment. Elle est puissamment secondée
par Vénus, qui amène Cupidon dans l'île avec ordre de percer de
ses flèches le coeur de Télémaque. Celui-ci, déjà blessé sans le
savoir, souhaite, sous divers prétextes de demeurer dans l'île,
malgré les sages remontrances de Mentor. Bientôt il sent pour la
nymphe Eucharis une folle passion, qui excite la jalousie et la
colère de Calypso. Elle jure par le Styx, que Télémaque sortira
de son île, et presse Mentor de construire un vaisseau pour le
reconduire à Ithaque. Tandis que Mentor entraîne Télémaque vers
le rivage pour s'embarquer, Cupidon va consoler Calypso, et
oblige les nymphes à brûler le vaisseau. A la vue des flammes,
Télémaque ressent une joie secrète; mais le sage Mentor, qui
s'en aperçoit, le précipite dans la mer, et s'y jette avec lui,
pour gagner à la nage un autre vaisseau alors arrêté auprès de
l'île de Calypso.
_________________________
Quand Télémaque eut achevé ce discours, toutes les nymphes, qui
avaient été immobiles, les yeux attachés sur lui, se regardèrent
les unes les autres. Elles se disaient avec étonnement: "Quels
sont donc ces deux hommes si chéris des dieux? A-t-on jamais ouï
parler d'aventures si merveilleuses? Le fils d'Ulysse le
surpasse déjà en éloquence, en sagesse et en valeur. Quelle
mine! Quelle beauté! Quelle douceur! Quelle modestie! Mais
quelle noblesse et quelle grandeur! Si nous ne savions qu'il est
le fils d'un mortel, on le prendrait aisément pour Bacchus, pour
Mercure, ou même pour le grand Apollon. Mais quel est ce Mentor,
qui paraît un homme simple, obscur et d'une médiocre condition?
Quand on le regarde de près, on trouve en lui je ne sais quoi
au-dessus de l'homme."
Calypso écoutait ces discours avec un trouble qu'elle ne pouvait
cacher: ses yeux errants allaient sans cesse de Mentor à
Télémaque, et de Télémaque à Mentor. Quelquefois elle voulait
que Télémaque recommençât cette longue histoire de ses
aventures; puis tout à coup elle s'interrompait elle-même.
Enfin, se levant brusquement, elle mena Télémaque seul dans un
bois de myrte, où elle n'oublia rien pour savoir de Mentor
n'était point une divinité cachée sous la forme d'un homme.
Télémaque ne pouvait le lui dire; car Minerve, en l'accompagnant
sous la figure de Mentor, ne s'était point découverte à lui à
cause de sa grande jeunesse. Elle ne se fiait pas encore assez à
son secret pour lui confier ses desseins. D'ailleurs elle
voulait l'éprouver par les plus grands dangers, et s'il eût su
que Minerve était avec lui, un tel secours l'eût trop soutenu:
il n'aurait eu aucune peine à mépriser les accidents les plus
affreux. Il prenait donc Minerve pour Mentor, et tous les
artifices de Calypso furent inutiles pour découvrir ce qu'elle
désirait savoir.
Cependant toutes les nymphes, assemblées autour de Mentor,
prenaient plaisir à le questionner. L'une lui demandait les
circonstances de son voyage d'Ethiopie; l'autre voulait savoir
ce qu'il avait vu à Damas; une autre lui demandait s'il avait
connu autrefois Ulysse avant le siège de Troie. Il répondait à
toutes avec douceur, et ses paroles, quoique simples, étaient
pleines de grâces.
Calypso ne les laissa pas longtemps dans cette conversation:
elle revint, et pendant que ses nymphes se mirent à cueillir des
fleurs en chantant pour amuser Télémaque, elle prit à l'écart
Mentor pour le faire parler. La douce vapeur du sommeil ne coule
pas plus doucement dans les yeux appesantis et dans tous les
membres fatigués d'un homme abattu que les paroles flatteuses de
la déesse s'insinuaient pour enchanter le coeur de Mentor; mais
elle sentait toujours je ne sais quoi qui repoussait tous ses
efforts et qui se jouait de ses charmes. Semblable à un rocher
escarpé qui cache son front dans les nues et qui se joue de la
rage des vents, Mentor, immobile dans ses sages desseins, se
laissait presser par Calypso. Quelquefois même il lui laissait
espérer qu'elle l'embarrasserait par ses questions et qu'elle
tirerait la vérité du fond de son coeur. Mais, au moment où elle
croyait satisfaire sa curiosité, ses espérances
s'évanouissaient: tout ce qu'elle s'imaginait tenir lui
échappait tout à coup, et une réponse courte de Mentor la
replongeait dans ses incertitudes.
Elle passait ainsi les journées, tantôt flattant Télémaque,
tantôt cherchant les moyens de le détacher de Mentor, qu'elle
n'espérait plus faire parler. Elle employait ses plus belles
nymphes à faire naître les feux de l'amour dans le coeur du
jeune Télémaque, et une divinité plus puissante qu'elle vint à
son secours pour y réussir.
Vénus, toujours pleine de ressentiment du mépris que Mentor et
Télémaque avaient témoigné pour le culte qu'on lui rendait dans
l'île de Chypre, ne pouvait se consoler de voir que ces deux
téméraires mortels eussent échappé aux vents et à la mer dans la
tempête excitée par Neptune. Elle en fit des plaintes amères à
Jupiter: mais le père des dieux, souriant, sans vouloir lui
découvrir que Minerve, sous la figure de Mentor, avait sauvé le
fils d'Ulysse, permit à Vénus de chercher les moyens de se
venger de ces deux hommes. Elle quitte l'Olympe; elle oublie les
doux parfums qu'on brûle sur ses autels à Paphos, à Cythère et à
Idalie; elle vole dans son char attelé de colombes; elle appelle
son fils, et, la douleur répandant sur son visage de nouvelles
grâces, elle parla ainsi:
- Vois-tu, mon fils, ces deux hommes qui méprisent ta puissance
et la mienne? Qui voudra désormais nous adorer? Va, perce de tes
flèches ces deux coeurs insensibles: descends avec moi dans
cette île; je parlerai à Calypso.
Elle dit, et, fendant les airs dans un nuage tout doré, elle se
présenta à Calypso, qui, dans ce moment, était seule au bord
d'une fontaine assez loin de sa grotte.
- Malheureuse déesse — lui dit-elle — l'ingrat Ulysse vous a
méprisée; son fils, encore plus dur que lui, vous prépare un
semblable mépris; mais l'Amour vient lui-même pour vous venger.
Je vous le laisse: il demeurera parmi vos nymphes, comme
autrefois l'enfant Bacchus fut nourri par les nymphes de l'île
de Naxos. Télémaque le verra comme un enfant ordinaire; il ne
pourra s'en défier, et il sentira bientôt son pouvoir.
Elle dit, et, remontant dans ce nuage doré d'où elle était
sortie, elle laissa après elle une odeur d'ambroisie dont tous
les bois de Calypso furent parfumés.
L'Amour demeura entre les bras de Calypso. Quoique déesse, elle
sentit la flamme qui coulait déjà dans son sein. Pour se
soulager, elle le donna aussitôt à la nymphe qui était auprès
d'elle, nommée Eucharis. Mais hélas! dans la suite, combien de
fois se repentit-elle de l'avoir fait!
D'abord rien ne paraissait plus innocent, plus doux, plus
aimable, plus ingénu et plus gracieux que cet enfant. A le voir
enjoué, flatteur, toujours riant, on aurait cru qu'il ne pouvait
donner que du plaisir: mais à peine s'était-on fié à ses
caresses, qu'on y sentait je ne sais quoi d'empoisonné. L'enfant
malin et trompeur ne caressait que pour trahir, et il ne riait
jamais que des maux cruels qu'il avait faits ou qu'il voulait
faire. Il n'osait approcher de Mentor, dont la sévérité
l'épouvantait, et il sentait que cet inconnu était invulnérable,
en sorte qu'aucune de ses flèches n'aurait pu le percer. Pour
les nymphes, elles sentirent bientôt les feux que cet enfant
trompeur allume; mais elles cachaient avec soin la plaie
profonde qui s'envenimait dans leurs coeurs.
Cependant Télémaque, voyant cet enfant qui se jouait avec les
nymphes, fut surpris de sa douceur et de sa beauté. Il
l'embrasse; il le prend tantôt sur ses genoux, tantôt entre ses
bras; il sent en lui-même une inquiétude dont il ne peut trouver
la cause. Plus il cherche à se jouer innocemment, plus il se
trouble et s'amollit.
- Voyez-vous ces nymphes? — disait-il à Mentor — combien
sont-elles différentes de ces femmes de l'île de Chypre, dont la
beauté était choquante à cause de leur immodestie! Ces beautés
immortelles montrent une innocence, une modestie, une simplicité
qui charme. Parlant ainsi, il rougissait sans savoir pourquoi.
Il ne pouvait s'empêcher de parler; mais à peine avait-il
commencé, qu'il ne pouvait continuer; ses paroles étaient
entrecoupées, obscures, et quelquefois elles n'avaient aucun
sens.
Mentor lui dit:
- O Télémaque, les dangers de l'île de Chypre n'étaient rien, si
on les compare à ceux dont vous ne vous défiez pas maintenant.
Le vice grossier fait horreur; l'impudence brutale donne de
l'indignation; mais la beauté modeste est bien plus dangereuse:
en l'aimant, on croit n'aimer que la vertu, et insensiblement on
se laisse aller aux appas trompeurs d'une passion qu'on
n'aperçoit que quand il n'est presque plus temps de l'éteindre.
Fuyez, ô mon cher Télémaque, fuyez ces nymphes, qui ne sont si
discrètes que pour vous mieux tromper; fuyez les dangers de
votre jeunesse: mais surtout fuyez cet enfant que vous ne
connaissez pas. C'est l'Amour, que Vénus, sa mère, est venue
apporter dans cette île, pour se venger du mépris que vous avez
témoigné pour le culte qu'on lui rend à Cythère. Il a blessé le
coeur de la déesse Calypso: elle est passionnée pour vous; il a
brûlé toutes les nymphes qui l'environnent; vous brûlez
vous-même, ô malheureux jeune homme, presque sans le savoir.
Télémaque interrompait souvent Mentor, en lui disant:
- Pourquoi ne demeurerions-nous pas dans cette île? Ulysse ne
vit plus: il doit être depuis longtemps enseveli dans les ondes;
Pénélope, ne voyant revenir ni lui ni moi, n'aura pu résister à
tant de prétendants: son père Icare l'aura contrainte d'accepter
un nouvel époux. Retournerai-je à Ithaque pour la voir engagée
dans de nouveaux liens et manquant à la foi qu'elle avait donnée
à mon père? Les Ithaciens ont oublié Ulysse. Nous ne pourrions y
retourner que pour chercher une mort assurée, puisque les amants
de Pénélope ont occupé toutes les avenues du port, pour mieux
assurer notre perte à notre retour.
Mentor répondait:
- Voilà l'effet d'une aveugle passion. On cherche avec subtilité
toutes les raisons qui la favorisent, et on se détourne de peur
de voir toutes celles qui la condamnent. On n'est plus ingénieux
que pour se tromper et pour étouffer ses remords. Avez-vous
oublié tout ce que les dieux ont fait pour vous ramener dans
votre patrie? Comment êtes-vous sorti de la Sicile? Les malheurs
que vous avez éprouvés en Egypte ne se sont-ils pas tournés tout
à coup en prospérités? Quelle main inconnue vous a enlevé à tous
les dangers qui menaçaient votre tête dans la ville de Tyr?
Après tant de merveilles, ignorez-vous encore ce que les
destinées vous ont préparé? Mais que dis-je? vous en êtes
indigne. Pour moi, je pars, et je saurai bien sortir de cette
île. Lâche fils d'un père si sage et si généreux, menez ici une
vie molle et sans honneur au milieu des femmes; faites, malgré
les dieux, ce que votre père crut indigne de lui.
Ces paroles de mépris percèrent Télémaque jusqu'au fond du
coeur. Il se sentait attendri pour Mentor; sa douleur était
mêlée de honte; il craignait l'indignation et le départ de cet
homme si sage, à qui il devait tant: mais une passion naissante,
et qu'il ne connaissait pas lui-même, faisait qu'il n'était plus
le même homme.
- Quoi donc! — disait-il à Mentor, les larmes aux yeux — vous ne
comptez pour rien l'immortalité qui m'est offerte par la déesse?
- Je compte pour rien — répondit Mentor — tout ce qui est contre
la vertu et contre les ordres des dieux. La vertu vous rappelle
dans votre patrie pour revoir Ulysse et Pénélope; la vertu vous
défend de vous abandonner à une folle passion. Les dieux, qui
vous ont délivré de tant de périls pour vous préparer une gloire
égale à celle de votre père, vous ordonnent de quitter cette
île. L'amour seul, ce honteux tyran, peut vous y retenir. Hé!
que feriez-vous d'une vie immortelle, sans liberté, sans vertu
et sans gloire? Cette vie serait encore plus malheureuse en ce
qu'elle ne pourrait finir.
Télémaque ne répondait à ce discours que par des soupirs.
Quelquefois il aurait souhaité que Mentor l'eût arraché malgré
lui de l'île; quelquefois il lui tardait que Mentor fût parti,
pour n'avoir plus devant ses yeux cet ami sévère qui lui
reprochait sa faiblesse. Toutes ces pensées contraires agitaient
tour à tour son coeur, et aucune n'y était constante: son coeur
était comme la mer, qui est le jouet de tous les vents
contraires. Il demeurait souvent étendu et immobile sur le
rivage de la mer, souvent dans le fond de quelque bois sombre,
versant des larmes amères et poussant des cris semblables aux
rugissements d'un lion. Il était devenu maigre; ses yeux creux
étaient pleins d'un feu dévorant; à le voir pâle, abattu et
défiguré, on aurait cru que ce n'était point Télémaque. Sa
beauté, son enjouement, sa noble fierté s'enfuyaient loin de
lui. Il périssait, tel qu'une fleur, qui, étant épanouie le
matin, répandait ses doux parfums dans la campagne et se flétrit
peu à peu vers le soir: ses vives couleurs s'effacent; elle
languit, elle se dessèche et sa belle tête se penche, ne pouvant
plus se soutenir; ainsi le fils d'Ulysse était aux portes de la
mort.
Mentor, voyant que Télémaque ne pouvait résister à la violence
de sa passion, conçut un dessein plein d'adresse pour le
délivrer d'un si grand danger. Il avait remarqué que Calypso
aimait éperdument Télémaque et que Télémaque n'aimait pas moins
la jeune nymphe Eucharis: car le cruel Amour, pour tourmenter
les mortels, fait qu'on n'aime guère la personne dont on est
aimé. Mentor résolut d'exciter la jalousie de Calypso.
Eucharis devait emmener Télémaque dans une chasse. Mentor dit à
Calypso:
- J'ai remarqué dans Télémaque une passion pour la chasse, que
je n'avais jamais vue en lui, ce plaisir commence à le dégoûter
de tout autre: il n'aime plus que les forêts et les montagnes
les plus sauvages. Est-ce vous, ô déesse, qui lui inspirez cette
grande ardeur?
Calypso sentit un dépit cruel en écoutant ces paroles, et elle
ne put se retenir.
- Ce Télémaque — répondit-elle — qui a méprisé tous les plaisirs
de l'île de Chypre, ne peut résister à la médiocre beauté d'une
de mes nymphes. Comment ose-t-il se vanter d'avoir fait tant
d'actions merveilleuses, lui dont le coeur s'amollit lâchement
par la volupté et qui ne semble né que pour passer une vie
obscure au milieu des femmes?
Mentor, remarquant avec plaisir combien la jalousie troublait le
coeur de Calypso, n'en dit pas davantage, de peur de la mettre
en défiance de lui; il lui montrait seulement un visage triste
et abattu. La déesse lui découvrait ses peines sur toutes les
choses qu'elle voyait, et elle faisait sans cesse des plaintes
nouvelles. Cette chasse, dont Mentor l'avait avertie, acheva de
la mettre en fureur. Elle sut que Télémaque n'avait cherché qu'à
se dérober aux autres nymphes pour parler à Eucharis. On
proposait même déjà une seconde chasse, où elle prévoyait qu'il
ferait comme dans la première. Pour rompre les mesures de
Télémaque, elle déclara qu'elle en voulait être. Puis, tout à
coup, ne pouvant plus modérer son ressentiment, elle lui parla
ainsi:
- Est-ce donc ainsi, ô jeune téméraire, que tu es venu dans mon
île pour échapper au juste naufrage que Neptune te préparait et
à la vengeance des dieux? N'es-tu entré dans cette île, qui
n'est ouverte à aucun mortel, que pour mépriser ma puissance et
l'amour que je t'ai témoigné? O divinités de l'Olympe et du
Styx, écoutez une malheureuse déesse: hâtez-vous de confondre ce
perfide, cet ingrat, cet impie. Puisque tu es encore plus dur et
plus injuste que ton père, puisses-tu souffrir des maux encore
plus longs et plus cruels que les siens! Non, non, que jamais tu
ne revoies ta patrie, cette pauvre et misérable Ithaque, que tu
n'as point eu honte de préférer à l'immortalité! Ou plutôt que
tu périsses, en la voyant de loin, au milieu de la mer; et que
ton corps, devenu le jouet des flots, soit rejeté, sans
espérance de sépulture, sur le sable de ce rivage! Que mes yeux
le voient mangé par les vautours! Celle que tu aimes le verra
aussi: elle le verra; elle en aura le coeur déchiré, et son
désespoir fera mon bonheur!
En parlant ainsi, Calypso avait les yeux rouges et enflammés:
ses regards ne s'arrêtaient jamais en aucun endroit; ils avaient
je ne sais quoi de sombre et de farouche. Ses joues tremblantes
étaient couvertes de taches noires et livides; elle changeait à
chaque moment de couleur. Souvent une pâleur mortelle se
répandait sur tout son visage; ses larmes ne coulaient plus,
comme autrefois, avec abondance: la rage et le désespoir
semblaient en avoir tari la source, et à peine en coulait-il
quelqu'une sur ses joues. Sa voix était rauque, tremblante et
entrecoupée. Mentor observait tous ses mouvements et ne parlait
plus à Télémaque. Il le traitait comme un malade désespéré qu'on
abandonne; il jetait souvent sur lui des regards de compassion.
Télémaque sentait combien il était coupable et indigne de
l'amitié de Mentor. Il n'osait lever les yeux, de peur de
rencontrer ceux de son ami, dont le silence même le condamnait.
Quelquefois il avait envie d'aller se jeter à son cou et de lui
témoigner combien il était touché de sa faute: mais il était
retenu, tantôt par une mauvaise honte, et tantôt par la crainte
d'aller plus loin qu'il ne voulait pour se tirer du péril: car
le péril lui semblait doux, et il ne pouvait encore se résoudre
à vaincre sa folle passion.
Les dieux et les déesses de l'Olympe, assemblés dans un profond
silence, avaient les yeux attachés sur l'île de Calypso, pour
voir qui serait victorieux, ou de Minerve ou de l'Amour.
L'Amour, en se jouant avec les nymphes, avait mis tout en feu
dans l'île; Minerve, sous la figure de Mentor, se servait de la
jalousie, inséparable de l'amour, contre l'Amour même. Jupiter
avait résolu d'être le spectateur de ce combat et de demeurer
neutre.
Cependant Eucharis, qui craignait que Télémaque ne lui échappât,
usait de mille artifices pour le retenir dans ses liens. Déjà
elle allait partir avec lui pour la seconde chasse, et elle
était vêtue comme Diane. Vénus et Cupidon avaient répandu sur
elle de nouveaux charmes, en sorte que ce jour-là sa beauté
effaçait celle de la déesse Calypso même. Calypso, la regardant
de loin, se regarda en même temps dans la plus claire de ses
fontaines, et elle eut honte de se voir. Alors elle se cacha au
fond de sa grotte et parla ainsi toute seule:
- Il ne me sert donc de rien d'avoir voulu troubler ces deux
amants, en déclarant que je veux être de cette chasse! En
serai-je? Irai-je la faire triompher et faire servir ma beauté à
relever la sienne? Faudra-t-il que Télémaque, en me voyant, soit
encore plus passionné pour son Eucharis? O malheureuse! qu'ai-je
fait? Non, je n'y irai pas, ils n'y iront pas eux-mêmes, je
saurai bien les en empêcher. Je vais trouver Mentor; je le
prierai d'enlever Télémaque: il le remmènera à Ithaque. Mais que
dis-je? et que deviendrai-je quand Télémaque sera parti? Où
suis-je? Que reste-t-il à faire? O cruelle Vénus, vous m'avez
trompée! O perfide présent que vous m'avez fait! Pernicieux
enfant, Amour empesté, je ne t'avais ouvert mon coeur que dans
l'espérance de vivre heureuse avec Télémaque, et tu n'as porté
dans ce coeur que trouble et que désespoir! Mes nymphes sont
révoltées contre moi. Ma divinité ne me sert plus qu'à rendre
mon malheur éternel. O si j'étais libre de me donner la mort
pour finir mes douleurs! Télémaque, il faut que tu meures,
puisque je ne puis mourir! Je me vengerai de tes ingratitudes:
ta nymphe le verra, et je te percerai à ses yeux. Mais je
m'égare. O malheureuse Calypso, que veux-tu? Faire périr un
innocent, que tu as jeté toi-même dans cet abîme de malheurs?
C'est moi qui ai mis le flambeau fatal dans le sein du chaste
Télémaque. Quelle innocence! Quelle vertu! Quelle horreur du
vice! Quel courage contre les honteux plaisirs! Fallait-il
empoisonner son coeur? Il m'eût quittée! Hé bien! ne faudra-t-il
pas qu'il me quitte, ou que je le voie, plein de mépris pour
moi, ne vivant plus que pour ma rivale? Non, non, je ne souffre
que ce que j'ai bien mérité. Pars, Télémaque, va-t'en au-delà
des mers; laisse Calypso sans consolation, ne pouvant supporter
la vie, ni trouver la mort: laisse-la inconsolable, couverte de
honte, désespérée, avec ton orgueilleuse Eucharis.
Elle parlait ainsi seule dans sa grotte: mais tout à coup elle
sort impétueusement.
- Où êtes-vous, ô Mentor? — dit-elle. — Est-ce ainsi que vous
soutenez Télémaque contre le vice auquel il succombe? Vous
dormez, pendant que l'Amour veille contre vous. Je ne puis
souffrir plus longtemps cette lâche indifférence que vous
témoignez. Verrez-vous toujours tranquillement le fils d'Ulysse
déshonorer son père et négliger sa haute destinée? Est-ce à vous
ou à moi que ses parents ont confié sa conduite? C'est moi qui
cherche les moyens de guérir son coeur; et vous, ne ferez-vous
rien? Il y a, dans le lieu le plus reculé de cette forêt, de
grands peupliers propres à construire un vaisseau; c'est là
qu'Ulysse fit celui dans lequel il sortit de cette île. Vous
trouverez au même endroit une profonde caverne, où sont tous les
instruments nécessaires pour tailler et pour joindre toutes les
pièces d'un vaisseau.
A peine eut-elle dit ces paroles, qu'elle s'en repentit. Mentor
ne perdit pas un moment: il alla dans cette caverne, trouva les
instruments, abattit les peupliers et mit en un seul jour un
vaisseau en état de voguer. C'est que la puissance et
l'industrie de Minerve n'ont pas besoin d'un grand temps pour
achever les plus grands ouvrages.
Calypso se trouva dans une horrible peine d'esprit: d'un côté,
elle voulait voir si le travail de Mentor s'avançait; de
l'autre, elle ne pouvait se résoudre à quitter la chasse, où
Eucharis aurait été en pleine liberté avec Télémaque. La
jalousie ne lui permit jamais de perdre de vue les deux amants:
mais elle tâchait de tourner la chasse du côté où elle savait
que Mentor faisait le vaisseau. Elle entendait les coups de
hache et de marteau: elle prêtait l'oreille; chaque coup la
faisait frémir. Mais, dans le moment même, elle craignait que
cette rêverie ne lui eût dérobé quelque signe ou quelque coup
d'oeil de Télémaque à la jeune nymphe.
Cependant Eucharis disait à Télémaque d'un ton moqueur:
- Ne craignez-vous point que Mentor ne vous blâme d'être venu à
la chasse sans lui? O que vous êtes à plaindre de vivre sous un
si rude maître! Rien ne peut adoucir son austérité: il affecte
d'être ennemi de tous les plaisirs; il ne peut souffrir que vous
en goûtiez aucun; il vous fait un crime des choses les plus
innocentes. Vous pouviez dépendre de lui pendant que vous étiez
hors d'état de vous conduire vous-même; mais après avoir montré
tant de sagesse, vous ne devez plus vous laisser traiter en
enfant.
Ces paroles artificieuses perçaient le coeur de Télémaque et le
remplissaient de dépit contre Mentor, dont il voulait secouer le
joug. Il craignait de le revoir et ne répondait rien à Eucharis,
tant il était troublé. Enfin, vers le soir, la chasse s'étant
passée de part et d'autre dans une contrainte perpétuelle, on
revint par un coin de la forêt assez voisin du lieu où Mentor
avait travaillé tout le jour. Calypso aperçut de loin le
vaisseau achevé; ses yeux se couvrirent à l'instant d'un épais
nuage, semblable à celui de la mort. Ses genoux tremblants se
dérobaient sous elle; une froide sueur courut par tous les
membres de son corps: elle fut contrainte de s'appuyer sur les
nymphes qui l'environnaient, et, Eucharis lui tendant la main
pour la soutenir, elle la repoussa en jetant sur elle un regard
terrible.
Télémaque, qui vit ce vaisseau, mais qui ne vit point Mentor,
parce qu'il s'était déjà retiré, ayant fini son travail, demanda
à la déesse à qui était ce vaisseau et à quoi on le destinait.
D'abord elle ne put répondre; mais enfin elle dit:
- C'est pour renvoyer Mentor que je l'ai fait faire; vous ne
serez plus embarrassé par cet ami sévère, qui s'oppose à votre
bonheur, et qui serait jaloux si vous deveniez immortel.
- Mentor m'abandonne! c'est fait de moi! s'écria Télémaque. — O
Eucharis, si Mentor me quitte, je n'ai plus que vous.
Ces paroles lui échappèrent dans le transport de sa passion. Il
vit le tort qu'il avait eu en les disant: mais il n'avait pas
été libre de penser au sens de ses paroles. Toute la troupe
étonnée demeura dans le silence. Eucharis, rougissant et
baissant les yeux, demeurait derrière, tout interdite, sans oser
se montrer. Mais pendant que la honte était sur son visage, la
joie était au fond de son coeur. Télémaque ne se comprenait plus
lui-même et ne pouvait croire qu'il eût parlé si indiscrètement.
Ce qu'il avait fait lui paraissait comme un songe, mais un songe
dont il demeurait confus et troublé.
Calypso, plus furieuse qu'une lionne à qui on a enlevé ses
petits, courait au travers de la forêt, sans suivre aucun
chemin, et ne sachant où elle allait. Enfin elle se trouva à
l'entrée de sa grotte, où Mentor l'attendait.
- Sortez de mon île — dit-elle — ô étrangers, qui êtes venus
troubler mon repos: loin de moi ce jeune insensé! Et vous,
imprudent vieillard, vous sentirez ce que peut le courroux d'une
déesse, si vous ne l'arrachez d'ici tout à l'heure. Je ne veux
plus le voir; je ne veux plus souffrir qu'aucune de mes nymphes
lui parle ni le regarde. J'en jure par les ondes du Styx,
serment qui fait trembler les dieux mêmes. Mais apprends,
Télémaque, que tes maux ne sont pas finis: ingrat, tu ne
sortiras de mon île que pour être en proie à de nouveaux
malheurs. Je serai vengée: tu regretteras Calypso, mais en vain.
Neptune, encore irrité contre ton père, qui l'a offensé en
Sicile, et sollicité par Vénus, que tu as méprisée dans l'île de
Chypre, te prépare d'autres tempêtes. Tu verras ton père, qui
n'est pas mort; mais tu le verras sans le connaître. Tu ne te
réuniras avec lui en Ithaque qu'après avoir été le jouet de la
plus cruelle fortune. Va: je conjure les puissances célestes de
me venger. Puisses-tu, au milieu des mers, suspendu aux pointes
d'un rocher et frappé de la foudre, invoquer en vain Calypso,
que ton supplice comblera de joie!
Ayant dit ces paroles, son esprit agité était déjà prêt à
prendre des résolutions contraires. L'amour rappela dans son
coeur le désir de retenir Télémaque. "Qu'il vive — disait-elle
en elle-même — qu'il demeure ici; peut-être qu'il sentira enfin
tout ce que j'ai fait pour lui. Eucharis ne saurait, comme moi,
lui donner l'immortalité. O trop aveugle Calypso, tu t'es trahie
toi-même par ton serment: te voilà engagée, et les ondes du
Styx, par lesquelles tu as juré, ne te permettent plus aucune
espérance." Personne n'entendait ces paroles: mais on voyait sur
son visage les Furies peintes, et tout le venin empesté du noir
Cocyte semblait s'exhaler de son coeur.
Télémaque en fut saisi d'horreur. Elle le comprit (car qu'est-ce
que l'amour jaloux ne devine pas?) et l'horreur de Télémaque
redoubla les transports de la déesse. Semblable à une bacchante
qui remplit l'air de ses hurlements et qui en fait retentir les
hautes montagnes de Thrace, elle court au travers des bois avec
un dard en main, appelant toutes ses nymphes et menaçant de
percer toutes celles qui ne la suivront pas. Elles courent en
foule, effrayées de cette menace. Eucharis même s'avance les
larmes aux yeux et regardant de loin Télémaque, à qui elle
n'osait plus parler. La déesse frémit en la voyant auprès
d'elle; et, loin de s'apaiser par la soumission de cette nymphe,
elle ressent une nouvelle fureur, voyant que l'affliction
augmente la beauté d'Eucharis.
Cependant Télémaque était demeuré seul avec Mentor. Il embrasse
ses genoux (car il n'osait l'embrasser autrement, ni le
regarder); il verse un torrent de larmes; il veut parier, la
voix lui manque; les paroles lui manquent encore davantage: il
ne sait ni ce qu'il doit faire, ni ce qu'il fait, ni ce qu'il
veut. Enfin il s'écrie:
- O mon vrai père, ô Mentor, délivrez-moi de tant de maux! Je ne
puis ni vous abandonner ni vous suivre. Délivrez-moi de tant de
maux, délivrez-moi de moi-même: donnez-moi la mort.
Mentor l'embrasse, le console, l'encourage, lui apprend à se
supporter lui-même, sans flatter sa passion, et lui dit:
- Fils du sage Ulysse, que les dieux ont tant aimé, et qu'ils
aiment encore, c'est par un effet de leur amour que vous
souffrez des maux si horribles. Celui qui n'a point senti sa
faiblesse et la violence de ses passions n'est point encore
sage; car il ne se connaît point encore et ne sait point se
défier de soi. Les dieux vous ont conduit comme par la main
jusqu'au bord de l'abîme, pour vous en montrer toute la
profondeur, sans vous y laisser tomber. Comprenez maintenant ce
que vous n'auriez jamais compris si vous ne l'aviez éprouvé. On
vous aurait parlé des trahisons de l'amour, qui flatte pour
perdre et qui, sous une apparence de douceur, cache les plus
affreuses amertumes. Il est venu, cet enfant plein de charmes,
parmi les ris, les jeux et les grâces. Vous l'avez vu; il a
enlevé votre coeur, et vous avez pris plaisir à le lui laisser
enlever. Vous cherchiez des prétextes pour ignorer la plaie de
votre coeur; vous cherchiez à me tromper et à vous flatter
vous-même; vous ne craigniez rien. Voyez le fruit de votre
témérité; vous demandez maintenant la mort, et c'est l'unique
espérance qui vous reste. La déesse troublée ressemble à une
Furie infernale; Eucharis brûle d'un feu plus cruel que toutes
les douleurs de la mort; toutes ces nymphes jalouses sont prêtes
à s'entre-déchirer: et voilà ce que fait le traître Amour, qui
paraît si doux! Rappelez tout votre courage. A quel point les
dieux vous aiment-ils, puisqu'ils vous ouvrent un si beau chemin
pour fuir l'Amour et pour revoir votre chère patrie! Calypso
elle-même est contrainte de vous chasser. Le vaisseau est tout
prêt: que tardons-nous à quitter cette île, où la vertu ne peut
habiter?
En disant ces paroles, Mentor le prit par la main et
l'entraînait vers le rivage. Télémaque suivait à peine,
regardant toujours derrière lui. Il considérait Eucharis, qui
s'éloignait de lui. Ne pouvant voir son visage, il regardait ses
beaux cheveux noués, ses habits flottants et sa noble démarche.
Il aurait voulu pouvoir baiser les traces de ses pas. Lors même
qu'il la perdit de vue, il prêtait encore l'oreille, s'imaginant
entendre sa voix. Quoique absente, il la voyait: elle était
peinte et comme vivante devant ses yeux; il croyait même parler
à elle, ne sachant plus où il était, et ne pouvant écouter
Mentor.
Enfin, revenant à lui comme d'un profond sommeil, il dit à
Mentor:
- Je suis résolu de vous suivre, mais je n'ai pas encore dit
adieu à Eucharis. J'aimerais mieux mourir que de l'abandonner
ainsi avec ingratitude. Attendez que je la revoie encore une
dernière fois pour lui faire un éternel adieu. Au moins souffrez
que je lui dise: "O nymphe, les dieux cruels, les dieux jaloux
de mon bonheur me contraignent de partir; mais ils m'empêcheront
plutôt de vivre que de me souvenir à jamais de vous." O mon
père, ou laissez-moi cette dernière consolation, qui est si
juste, ou arrachez-moi la vie dans ce moment. Non, je ne veux ni
demeurer dans cette île, ni m'abandonner à l'amour. L'amour
n'est point dans mon coeur; je ne sens que de l'amitié et de la
reconnaissance pour Eucharis. Il me suffit de lui dire adieu
encore une fois, et je pars avec vous sans retardement!
- Que j'ai pitié de vous! — répondait Mentor — votre passion est
si furieuse que vous ne la sentez pas. Vous croyez être
tranquille, et vous demandez la mort! Vous osez dire que vous
n'êtes point vaincu par l'amour, et vous ne pouvez vous arracher
à la nymphe que vous aimez! Vous ne voyez, vous n'entendez
qu'elle; vous êtes aveugle et sourd à tout le reste. Un homme
que la fièvre rend frénétique dit: "Je ne suis pas malade". O
aveugle Télémaque, vous étiez prêt à renoncer à Pénélope, qui
vous attend, à Ulysse, que vous verrez, à Ithaque où vous devez
régner, à la gloire et à la haute destinée que les dieux vous
ont promise par tant de merveilles qu'ils ont faites en votre
faveur; vous renonciez à tous ces biens pour vivre déshonoré
auprès d'Eucharis: direz-vous encore que l'amour ne vous attache
point à elle? Qu'est-ce donc qui vous trouble? Pourquoi
voulez-vous mourir? Pourquoi avez-vous parlé devant la déesse
avec tant de transport? Je ne vous accuse point de mauvaise foi;
mais je déplore votre aveuglément. Fuyez, Télémaque, fuyez: on
ne peut vaincre l'amour qu'en fuyant. Contre un tel ennemi, le
vrai courage consiste à craindre et à fuir, mais à fuir sans
délibérer et sans se donner à soi-même le temps de regarder
jamais derrière soi. Vous n'avez pas oublié les soins que vous
m'avez coûtés depuis votre enfance et les périls dont vous êtes
sorti par mes conseils: ou croyez-moi, ou souffrez que je vous
abandonne. Si vous saviez combien il m'est douloureux de vous
voir courir à votre perte! Si vous saviez tout ce que j'ai
souffert pendant que je n'ai osé vous parler! La mère qui vous
mit au monde souffrit moins dans les douleurs de l'enfantement.
Je me suis tu; j'ai dévoré ma peine; j'ai étouffé mes soupirs,
pour voir si vous reviendriez à moi. O mon fils, mon cher fils,
soulagez mon coeur; rendez-moi ce qui m'est plus cher que mes
entrailles: rendez-moi Télémaque, que j'ai perdu; rendez-vous à
vous-même. Si la sagesse en vous surmonte l'amour, je vis et je
vis heureux; mais si l'amour vous entraîne malgré la sagesse,
Mentor ne peut plus vivre.
Pendant que Mentor parlait ainsi, il continuait son chemin vers
la mer; et Télémaque, qui n'était pas encore assez fort pour le
suivre de lui-même, l'était déjà assez pour se laisser mener
sans résistance. Minerve, toujours cachée sous la figure de
Mentor, couvrant invisiblement Télémaque de son égide et
répandant autour de lui un rayon divin, lui fit sentir un
courage qu'il n'avait point encore éprouvé depuis qu'il était
dans cette île. Enfin ils arrivèrent dans un endroit de l'île où
le rivage de la mer était escarpé: c'était un rocher toujours
battu par l'onde écumante. Ils regardèrent de cette hauteur si
le vaisseau que Mentor avait préparé était encore dans la même
place; mais ils aperçurent un triste spectacle.
L'Amour était vivement piqué de voir que ce vieillard inconnu
non seulement était insensible à ses traits, mais encore lui
enlevait Télémaque: il pleurait de dépit, et il alla trouver
Calypso errante dans les sombres forêts. Elle ne put le voir
sans gémir, et elle sentit qu'il rouvrait toutes les plaies de
son coeur. L'Amour lui dit:
- Vous êtes déesse, et vous vous laissez vaincre par un faible
mortel, qui est captif dans votre île! Pourquoi le laissez-vous
sortir?
- O malheureux Amour — répondit-elle — je ne veux plus écouter
tes pernicieux conseils: c'est toi qui m'as tirée d'une douce et
profonde paix, pour me précipiter dans un abîme de malheurs.
C'en est fait; j'ai juré par les ondes du Styx que je laisserais
partir Télémaque: Jupiter même, le père des dieux, avec toute sa
puissance, n'oserait contrevenir à ce redoutable serment.
Télémaque sort de mon île; sors aussi, pernicieux enfant: tu
m'as fait plus de mal que lui!
L'Amour, essuyant ses larmes, fit un sourire moqueur et malin.
- En vérité — dit-il — voilà un grand embarras! Laissez-moi
faire. Suivez votre serment; ne vous opposez point au départ de
Télémaque. Ni vos nymphes, ni moi n'avons juré par les ondes du
Styx de le laisser partir: je leur inspirerai le dessein de
brûler ce vaisseau, que Mentor a fait avec tant de
précipitation. Sa diligence, qui nous a surpris, sera inutile.
Il sera surpris lui-même à son tour, et il ne lui restera plus
aucun moyen de vous arracher Télémaque.
Ces paroles flatteuses firent glisser l'espérance et la joie
jusqu'au fond des entrailles de Calypso. Ce qu'un zéphyr fait
par sa fraîcheur sur le bord d'un ruisseau, pour délasser les
troupeaux languissants que l'ardeur de l'été consume, ce
discours le fit pour apaiser le désespoir de la déesse. Son
visage devint serein, ses yeux s'adoucirent, les noirs soucis
qui rongeaient son coeur s'enfuirent pour un moment loin d'elle:
elle s'arrêta, elle sourit, elle flatta le folâtre Amour: et, en
le flattant, elle se prépara de nouvelles douleurs.
L'Amour, content de l'avoir persuadée, alla pour persuader aussi
les Nymphes, qui étaient errantes et dispersées sur toutes les
montagnes, comme un troupeau de moutons que la rage des loups
affamés a mis en fuite loin du berger. L'Amour les rassemble et
leur dit:
- Télémaque est encore en vos mains; hâtez-vous de brûler ce
vaisseau, que le téméraire Mentor a fait pour s'enfuir.
Aussitôt elles allument des flambeaux; elles accourent sur le
rivage; elles frémissent; elles poussent des hurlements; elles
secouent leurs cheveux épars, comme des bacchantes. Déjà la
flamme vole; elle dévore le vaisseau, qui est d'un bois sec et
enduit de résine; des tourbillons de fumée et de flamme
s'élèvent dans les nues.
Télémaque et Mentor aperçoivent le feu de dessus le rocher, et
entendent les cris des Nymphes. Télémaque fut tenté de s'en
réjouir; car son coeur n'était pas encore guéri, et Mentor
remarquait que sa passion était comme un feu mal éteint, qui
sort de temps en temps de dessous la cendre et qui repousse de
vives étincelles.
- Me voilà donc — dit Télémaque — rengagé dans mes liens! Il ne
nous reste plus aucune espérance de quitter cette île.
Mentor vit bien que Télémaque allait retomber dans toutes ses
faiblesses et qu'il n'y avait pas un seul moment à perdre. Il
aperçut de loin, au milieu des flots, un vaisseau arrêté, qui
n'osait approcher de l'île, parce que tous les pilotes
connaissaient que l'île de Calypso était inaccessible à tous les
mortels. Aussitôt le sage Mentor, poussant Télémaque, qui était
assis sur le bord du rocher, le précipite dans la mer et s'y
jette avec lui. Télémaque, surpris de cette violente chute, but
l'onde amère et devint le jouet des flots. Mais, revenant à lui
et voyant Mentor qui lui tendait la main pour lui aider à nager,
il ne songea plus qu'à s'éloigner de l'île fatale.
Les Nymphes, qui avaient cru les tenir captifs, poussèrent des
cris pleins de fureur, ne pouvant plus empêcher leur fuite.
Calypso, inconsolable, rentra dans sa grotte, qu'elle remplit de
ses hurlements. L'Amour, qui vit changer son triomphe en une
honteuse défaite, s'éleva au milieu de l'air en secouant ses
ailes et s'envola dans le bocage d'Idalie, où sa cruelle mère
l'attendait. L'enfant, encore plus cruel, ne se consola qu'en
riant avec elle de tous les maux qu'il avait faits.
A mesure que Télémaque s'éloignait de l'île, il sentait avec
plaisir renaître son courage, et son amour pour la vertu.
- J'éprouve — s'écriait-il parlant à Mentor — ce que vous me
disiez et que je ne pouvais croire, faute d'expérience: on ne
surmonte le vice qu'en le fuyant. O mon père, que les dieux
m'ont aimé en me donnant votre secours! Je méritais d'en être
privé et d'être abandonné à moi-même. Je ne crains plus ni mers,
ni vents, ni tempêtes; je ne crains plus que mes passions.
L'amour est lui seul plus à craindre que tous les naufrages.

 
 
 

<<< sommaire  <<< page précédente  page suivante >>>
