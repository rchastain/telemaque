<<< sommaire  <<< page précédente  page suivante >>>

B  I  B  L  I  O  T  H  E  C  A    A  U  G  U  S  T  A  N  A

 

 

 

 

Fénelon
Les aventures de Télémaque

 
 

 

Q u a t r i è m e  l i v r e
_________________________
Sommaire de l'édition dite de Versailles (1824)
Calypso interrompt Télémaque pour refaire reposer. Mentor le
blâme en secret d'avoir entrepris le récit de ses aventures, et
cependant lui conseille de l'achever, puisqu'il l'a commencé.
Télémaque, selon l'avis de Mentor, continue son récit. Pendant
le trajet de Tyr à l'île de Chypre, il voit en songe Vénus et
Cupidon l'inviter au plaisir. Minerve lui apparaît aussi, le
protégeant de son égide, et Mentor, l'exhortant à fuir de l'île
de Chypre. A son réveil, les Chypriens, noyés dans le vin, sont
surpris dans une furieuse tempête, qui eût lait périr le navire,
si Télémaque lui-même n'eût pris en main le gouvernail et
commandé les manoeuvres. Enfin on arrive dans l'île. Peintures
des moeurs voluptueuses de ses habitants, du culte rendu à
Vénus, et des impressions funestes que ce spectacle produit sur
le coeur de Télémaque. Les sages conseils de Mentor, qu'il
retrouve tout à coup en ce lieu, le délivrent d'un si grand
danger. Le Syrien Hasaël, à qui Mentor avait été vendu, ayant
été contraint par les vents de relâcher à l'île de Chypre, comme
il allait en Crète pour y étudier les lois de Minos, rend à
Télémaque son sage conducteur, et s'embarque avec eux pour l'île
de Crète. Ils jouissent, dans ce trajet, du beau spectacle
d'Amphitrite traînée dans son char par des chevaux marins.
_________________________
Calypso, qui avait été jusqu'à ce moment immobile et transportée
de plaisir en écoutant les aventures de Télémaque, l'interrompit
pour lui faire prendre quelque repos.
- Il est temps — lui dit-elle — que vous alliez goûter la
douceur du sommeil après tant de travaux. Vous n'avez rien à
craindre ici: tout vous est favorable. Abandonnez-vous donc à la
joie; goûtez la paix et tous les autres dons des dieux, dont
vous allez être comblé. Demain, quand l'Aurore avec ses doigts
de roses entrouvrira les portes dorées de l'orient et que les
chevaux du soleil, sortant de l'onde amère, répandront les
flammes du jour pour chasser devant eux toutes les étoiles du
ciel, nous reprendrons, mon cher Télémaque, l'histoire de vos
malheurs. Jamais votre père n'a égalé votre sagesse et votre
courage; ni Achille, vainqueur d'Hector, ni Thésée, revenu des
enfers, ni même le grand Alcide, qui a purgé la terre de tant de
monstres, n'ont fait voir autant de force et de vertu que vous.
Je souhaite qu'un profond sommeil rende cette nuit courte pour
vous. Mais, hélas! qu'elle sera longue pour moi! Qu'il me
tardera de vous revoir, de vous entendre, de vous faire redire
ce que je sais déjà et de vous demander ce que je ne sais pas
encore! Allez, mon cher Télémaque, avec le sage Mentor, que les
dieux vous ont rendu; allez dans cette grotte écartée, où tout
est préparé pour votre repos. Je prie Morphée de répandre ses
plus doux charmes sur vos paupières appesanties, de faire couler
une vapeur divine dans tous vos membres fatigués et de vous
envoyer des songes légers, qui, voltigeant autour de vous,
flattent vos sens par les images les plus riantes et repoussent
loin de vous tout ce qui pourrait vous réveiller trop
promptement.
La déesse conduisit elle-même Télémaque dans cette grotte
séparée de la sienne. Elle n'était ni moins rustique, ni moins
agréable. Une fontaine, qui coulait dans un coin, y faisait un
doux murmure, qui appelait le sommeil. Les nymphes y avaient
préparé deux lits d'une molle verdure, sur lesquels elles
avaient étendu deux grandes peaux, l'une de lion pour Télémaque,
et l'autre d'ours pour Mentor.
Avant que de laisser fermer ses yeux au sommeil, Mentor parla
ainsi à Télémaque:
- Le plaisir de raconter vos histoires vous a entraîné; vous
avez charmé la déesse en lui expliquant les dangers dont votre
courage et votre industrie vous ont tiré: par là vous n'avez
fait qu'enflammer davantage son coeur et que vous préparer une
plus dangereuse captivité. Comment espérez-vous qu'elle vous
laisse maintenant sortir de son île, vous qui l'avez enchantée
par le récit de vos aventures? L'amour d'une vaine gloire vous a
fait parler sans prudence. Elle s'était engagée à vous raconter
des histoires et à vous apprendre quelle a été la destinée
d'Ulysse; elle a trouvé moyen de parler longtemps sans rien
dire, et elle vous a engagé à lui expliquer tout ce qu'elle
désire savoir: tel est l'art des femmes flatteuses et
passionnées. Quand est-ce, ô Télémaque, que vous serez assez
sage pour ne parler jamais par vanité et que vous saurez taire
tout ce qui vous est avantageux, quand il n'est pas utile à
dire? Les autres admirent votre sagesse dans un âge où il est
pardonnable d'en manquer; pour moi, je ne puis vous pardonner
rien: je suis le seul qui vous connaît, et qui vous aime assez
pour vous avertir de toutes vos fautes. Combien êtes-vous encore
éloigné de la sagesse de votre père!
- Quoi donc! — répondit Télémaque — pouvais-je refuser à Calypso
de lui raconter mes malheurs?
- Non — reprit Mentor — il fallait les lui raconter: mais vous
deviez le faire en ne lui disant que ce qui pouvait lui donner
de la compassion. Vous pouviez dire que vous aviez été tantôt
errant, tantôt captif en Sicile, et puis en Egypte. C'était lui
dire assez, et tout le reste n'a servi qu'à augmenter le poison
qui brûle déjà son coeur. Plaise aux dieux que le vôtre puisse
s'en préserver!
- Mais que ferai-je donc? — continua Télémaque d'un ton modéré
et docile.
- Il n'est plus temps — repartit Mentor — de lui cacher ce qui
reste de vos aventures: elle en sait assez pour ne pouvoir être
trompée sur ce qu'elle ne sait pas encore; votre réserve ne
servirait qu'à l'irriter. Achevez donc demain de lui raconter
tout ce que les dieux ont fait en votre faveur, et apprenez une
autre fois à parler plus sobrement de tout ce qui peut vous
attirer quelque louange.
Télémaque reçut avec amitié un si bon conseil, et ils se
couchèrent.
Aussitôt que Phébus eut répandu ses premiers rayons sur la
terre, Mentor, entendant la voix de la déesse qui appelait ses
nymphes dans le bois, éveilla Télémaque.
- Il est temps — lui dit-il — de vaincre le sommeil. Allons
retrouver Calypso: mais défiez-vous de ses douces paroles; ne
lui ouvrez jamais votre coeur; craignez le poison flatteur de
ses louanges. Hier elle vous élevait au-dessus de votre sage
père, de l'invincible Achille, du fameux Thésée, d'Hercule
devenu immortel. Sentîtes-vous combien cette louange est
excessive? Crûtes-vous ce qu'elle disait? Sachez qu'elle ne le
croit pas elle-même: elle ne vous loue qu'à cause qu'elle vous
croit faible et assez vain pour vous laisser tromper par des
louanges disproportionnées à vos actions.
Après ces paroles, ils allèrent au lieu où la déesse les
attendait. Elle sourit en les voyant et cacha sous une apparence
de joie la crainte et l'inquiétude qui troublaient son coeur:
car elle prévoyait que Télémaque, conduit par Mentor, lui
échapperait de même qu'Ulysse.
- Hâtez-vous — dit-elle — mon cher Télémaque, de satisfaire ma
curiosité: j'ai cru, pendant toute la nuit, vous voir partir de
Phénicie et chercher une nouvelle destinée dans l'île de Chypre.
Dites-nous donc quel fut ce voyage et ne perdons pas un moment.
Alors on s'assit sur l'herbe semée de violettes, à l'ombre d'un
bocage épais.
Calypso ne pouvait s'empêcher de jeter sans cesse des regards
tendres et passionnés sur Télémaque et de voir avec indignation
que Mentor observait jusqu'au moindre mouvement de ses yeux.
Cependant toutes les nymphes en silence se penchaient pour
prêter l'oreille et faisaient une espèce de demi-cercle pour
mieux écouter et pour mieux voir: les yeux de toute l'assemblée
étaient immobiles et attachés sur ce jeune homme. Télémaque,
baissant les yeux et rougissant avec beaucoup de grâce, reprit
ainsi la suite de son histoire:
"A peine le doux souffle d'un vent favorable avait rempli nos
voiles, que la terre de Phénicie disparut à nos yeux. Comme
j'étais avec les Chypriens, dont j'ignorais les moeurs, je me
résolus de me taire, de remarquer tout et d'observer toutes les
règles de la discrétion pour gagner leur estime. Mais, pendant
mon silence, un sommeil doux et puissant vint me saisir: mes
sens étaient liés et suspendus; je goûtais une paix et une joie
profonde qui enivrait mon coeur.
Tout à coup, je crus voir Vénus, qui fendait les nues dans son
char volant conduit par deux colombes. Elle avait cette
éclatante beauté, cette vive jeunesse, ces grâces tendres, qui
parurent en elle quand elle sortit de l'écume de l'Océan et
qu'elle éblouit les yeux de Jupiter même. Elle descendit tout à
coup d'un vol rapide jusqu'auprès de moi, me mit en souriant la
main sur l'épaule, et, me nommant par mon nom, prononça ces
paroles: "Jeune Grec, tu vas entrer dans mon empire; tu
arriveras bientôt dans cette île fortunée où les plaisirs, les
ris et les jeux folâtres naissent sous mes pas. Là, tu brûleras
des parfums sur mes autels; là je te plongerai dans un fleuve de
délices. Ouvre ton coeur aux plus douces espérances, et
garde-toi bien de résister à la plus puissante de toutes les
déesses, qui veut te rendre heureux."
En même temps j'aperçus l'enfant Cupidon, dont les petites ailes
s'agitant le faisaient voler autour de sa mère. Quoiqu'il eût
sur son visage la tendresse, les grâces et l'enjouement de
l'enfance, il avait je ne sais quoi dans ses yeux perçants qui
me faisait peur. Il riait en me regardant; son ris était malin,
moqueur et cruel. Il tira de son carquois d'or la plus aiguë de
ses flèches, il banda son arc, et allait me percer, quand
Minerve se montra soudainement pour me couvrir de son égide. Le
visage de cette déesse n'avait point cette beauté molle et cette
langueur passionnée que j'avais remarquée dans le visage et dans
la posture de Vénus. C'était au contraire une beauté simple,
négligée, modeste; tout était grave, vigoureux, noble, plein de
force et de majesté. La flèche de Cupidon, ne pouvant percer
l'égide, tomba par terre. Cupidon indigné en soupira amèrement;
il eut honte de se voir vaincu. "Loin d'ici, s'écria Minerve,
loin d'ici, téméraire enfant! Tu ne vaincras jamais que des âmes
lâches, qui aiment mieux tes honteux plaisirs que la sagesse, la
vertu et la gloire." A ces mots, l'Amour irrité s'envola, et,
Vénus remontant vers l'Olympe, je vis longtemps son char avec
ses deux colombes dans une nuée d'or et d'azur, puis elle
disparut. En baissant mes yeux vers la terre, je ne retrouvai
plus Minerve.
Il me sembla que j'étais transporté dans un jardin délicieux,
tel qu'on dépeint les Champs Elysées. En ce lieu je reconnus
Mentor, qui me dit: "Fuyez cette cruelle terre, cette île
empestée, où l'on ne respire que la volupté. La vertu la plus
courageuse y doit trembler, et ne se peut sauver qu'en fuyant."
Dès que je le vis, je voulus me jeter à son cou pour
l'embrasser; mais je sentais que mes pieds ne pouvaient se
mouvoir, que mes genoux se dérobaient sous moi, et que mes
mains, s'efforçant de saisir Mentor, cherchaient une ombre vaine
qui m'échappait toujours.
Dans cet effort je m'éveillai, et je sentis que ce songe
mystérieux était un avertissement divin. Je me sentis plein de
courage contre les plaisirs, et de défiance contre moi-même,
pour détester la vie molle des Chypriens. Mais ce qui me perça
le coeur fut que je crus que Mentor avait perdu la vie et
qu'ayant passé les ondes du Styx il habitait l'heureux séjour
des âmes justes.
Cette pensée me fit répandre un torrent de larmes. On me demanda
pourquoi je pleurais.
"Les larmes, — répondis-je — ne conviennent que trop à un
malheureux étranger qui erre sans espérance de revoir sa
patrie."
Cependant tous ces Chypriens qui étaient dans le vaisseau
s'abandonnaient à une folle joie. Les rameurs, ennemis du
travail, s'endormaient sur leurs rames; le pilote, couronné de
fleurs, laissait le gouvernail et tenait en sa main une grande
cruche de vin, qu'il avait presque vidée: lui et tous les
autres, troublés par la fureur de Bacchus, chantaient en
l'honneur de Vénus et de Cupidon, des vers qui devaient faire
horreur à tous ceux qui aiment la vertu. Pendant qu'ils
oubliaient ainsi les dangers de la mer, une soudaine tempête
troubla le ciel et la mer. Les vents déchaînés mugissaient avec
fureur dans les voiles, les ondes noires battaient les flancs du
navire, qui gémissait sous leurs coups. Tantôt nous montions sur
le dos des vagues enflées; tantôt la mer semblait se dérober
sous le navire et nous précipiter dans l'abîme. Nous apercevions
auprès de nous des rochers contre lesquels les flots irrités se
brisaient avec un bruit horrible.
Alors je compris par expérience ce que j'avais souvent ouï dire
à Mentor, que les hommes mous et abandonnés aux plaisirs
manquent de courage dans les dangers. Tous nos Chypriens abattus
pleuraient comme des femmes; je n'entendais que des cris
pitoyables, que des regrets sur les délices de la vie, que de
vaines promesses aux dieux pour leur faire des sacrifices, si on
pouvait arriver au port. Personne ne conservait assez de
présence d'esprit ni pour ordonner les manoeuvres, ni pour les
faire. Il me parut que je devais, en sauvant ma vie, sauver
celle des autres. Je pris le gouvernail en main, parce que le
pilote, troublé par le vin comme une bacchante, était hors
d'état de connaître le danger du vaisseau. J'encourageai les
matelots effrayés; je leur fis abaisser les voiles: ils ramèrent
vigoureusement; nous passâmes au travers des écueils, et nous
vîmes de près toutes les horreurs de la mort.
Cette aventure parut comme un songe à tous ceux qui me devaient
la conservation de leur vie; ils me regardaient avec étonnement.
Nous arrivâmes dans l'île de Chypre au mois du printemps qui est
consacré à Vénus. Cette saison, disaient les Chypriens, convient
à cette déesse; car elle semble ranimer toute la nature et faire
naître les plaisirs comme les fleurs.
En arrivant dans l'île, je sentis un air doux qui rendait les
corps lâches et paresseux, mais qui inspirait une humeur enjouée
et folâtre. Je remarquai que la campagne, naturellement fertile
et agréable, était presque inculte, tant les habitants étaient
ennemis du travail. Je vis de tous côtés des femmes et des
jeunes filles vainement parées, qui allaient, en chantant les
louanges de Vénus, se dévouer à son temple. La beauté, les
grâces, la joie, les plaisirs éclataient également sur leurs
visages: mais les grâces y étaient affectées; on n'y voyait
point une noble simplicité et une pudeur aimable, qui fait le
plus grand charme de la beauté. L'air de mollesse, l'art de
composer leurs visages, leur parure vaine, leur démarche
languissante, leurs regards qui semblaient chercher ceux des
hommes, leur jalousie entre elles pour allumer de grandes
passions, en un mot, tout ce que je voyais dans ces femmes me
semblait vil et méprisable: à force de vouloir plaire, elles me
dégoûtaient.
On me conduisit au temple de la déesse: elle en a plusieurs dans
cette île; car elle est particulièrement adorée à Cythère, à
Idalie et à Paphos. C'est à Cythère que je fus conduit. Le
temple est tout de marbre: c'est un parfait péristyle; les
colonnes sont d'une grosseur et d'une hauteur qui rendent cet
édifice très majestueux; au-dessus de l'architrave et de la
frise sont à chaque face de grands frontons où l'on voit en
bas-reliefs toutes les plus agréables aventures de la déesse. A
la porte du temple est sans cesse une foule de peuples qui
viennent faire leurs offrandes. On n'égorge jamais dans
l'enceinte du lieu sacré aucune victime; on n'y brûle point,
comme ailleurs, la graisse des génisses et des taureaux; on ne
répand jamais leur sang: on présente seulement devant l'autel
les bêtes qu'on offre, et on n'en peut offrir aucune qui ne soit
jeune, blanche, sans défaut et sans tache. On les couvre de
bandelettes de pourpre brodées d'or; leurs cornes sont dorées et
ornées de bouquets de fleurs les plus odoriférantes. Après
qu'elles ont été présentées devant l'autel, on les renvoie dans
un lieu écarté, où elles sont égorgées pour les festins des
prêtres de la déesse.
On offre aussi toutes sortes de liqueurs parfumées et du vin
plus doux que le nectar. Les prêtres sont revêtus de longues
robes blanches, avec des ceintures d'or, et des franges de même
au bas de leurs robes. On brûle nuit et jour, sur les autels,
les parfums les plus exquis de l'Orient, et ils forment une
espèce de nuage qui monte vers le ciel. Toutes les colonnes du
temple sont ornées de festons pendants; tous les vases qui
servent aux sacrifices sont d'or. Un bois sacré de myrtes
environne le bâtiment. Il n'y a que de jeunes garçons et de
jeunes filles d'une rare beauté qui puissent présenter les
victimes aux prêtres et qui osent allumer le feu des autels.
Mais l'impudence et la dissolution déshonorent un temple si
magnifique.
D'abord, j'eus horreur de tout ce que je voyais; mais
insensiblement je commençais à m'y accoutumer. Le vice ne
m'effrayait plus; toutes les compagnies m'inspiraient je ne sais
quelle inclination pour le désordre: on se moquait de mon
innocence; ma retenue et ma pudeur servaient de jouet à ces
peuples effrontés. On n'oubliait rien pour exciter toutes mes
passions, pour me tendre des pièges et pour réveiller en moi le
goût des plaisirs. Je me sentais affaiblir tous les jours; la
bonne éducation que j'avais reçue ne me soutenait presque plus;
toutes mes bonnes résolutions s'évanouissaient. Je ne me sentais
plus la force de résister au mal, qui me pressait de tous côtés;
j'avais même une mauvaise honte de la vertu. J'étais comme un
homme qui nage dans une rivière profonde et rapide: d'abord il
fend les eaux et remonte contre le torrent; mais, si les bords
sont escarpés et s'il ne peut se reposer sur le rivage, il se
lasse enfin peu à peu; sa force l'abandonne, ses membres épuisés
s'engourdissent, et le cours du fleuve l'entraîne. Ainsi, mes
yeux commençaient à s'obscurcir, mon coeur tombait en
défaillance; je ne pouvais plus rappeler ni ma raison, ni le
souvenir des vertus de mon père. Le songe où je croyais avoir vu
le sage Mentor descendu aux Champs Elysées achevait de me
décourager: une secrète et douce langueur s'emparait de moi;
j'aimais déjà le poison flatteur qui se glissait de veine en
veine et qui pénétrait jusqu'à la moelle de mes os.
Je poussais néanmoins encore de profonds soupirs; je versais des
larmes amères; je rugissais comme un lion, dans ma fureur. "O
malheureuse jeunesse! — disais-je -: ô dieux, qui vous jouez
cruellement des hommes, pourquoi les faites-vous passer par cet
âge, qui est un temps de folie et de fièvre ardente? O que ne
suis-je couvert de cheveux blancs, courbé et proche du tombeau,
comme Laërte mon aïeul! La mort me serait plus douce que la
faiblesse honteuse où je me vois."
A peine avais-je ainsi parlé que ma douleur s'adoucissait et que
mon coeur, enivré d'une folle passion, secouait presque toute
pudeur; puis je me voyais replongé dans un abîme de remords.
Pendant ce trouble, je courais errant çà et là dans le sacré
bocage, semblable à une biche qu'un chasseur a blessée; elle
court au travers des vastes forêts pour soulager sa douleur;
mais la flèche qui l'a percée dans le flanc la suit partout;
elle porte partout avec elle le trait meurtrier. Ainsi je
courais en vain pour m'oublier moi-même et rien n'adoucissait la
plaie de mon coeur.
En ce moment, j'aperçus assez loin de moi, dans l'ombre épaisse
de ce bois, la figure du sage Mentor; mais son visage me parut
si pâle, si triste et si austère, que je ne pus en ressentir
aucune joie.
"Est-ce donc vous — m'écriai-je — ô mon cher ami, mon unique
espérance, est-ce vous? Quoi donc! est-ce vous-même? Une image
trompeuse ne vient-elle point abuser mes yeux? Est-ce vous,
Mentor? N'est-ce point votre ombre, encore sensible à mes maux?
N'êtes-vous point au rang des âmes heureuses qui jouissent de
leur vertu et à qui les dieux donnent des plaisirs purs dans une
éternelle paix aux Champs Elysées? Parlez, Mentor: vivez-vous
encore? Suis-je assez heureux pour vous posséder, ou bien
n'est-ce qu'une ombre de mon ami?" En disant ces paroles, je
courais vers lui, tout transporté, jusqu'à perdre la
respiration; il m'attendait tranquillement sans faire un pas
vers moi. O Dieux, vous le savez, quelle fut ma joie quand je
sentis que mes bras le touchaient!
"Non, ce n'est pas une vaine ombre! Je le tiens, je l'embrasse,
mon cher Mentor!"
C'est ainsi que je m'écriai. J'arrosai son visage d'un torrent
de larmes; je demeurais attaché à son cou sans pouvoir parler.
Il me regardait tristement avec des yeux pleins d'une tendre
compassion.
Enfin je lui dis:
"Hélas! d'où venez-vous! En quels dangers ne m'avez-vous point
laissé pendant votre absence! Et que ferais-je maintenant sans
vous?"
Mais, sans répondre à mes questions:
"Fuyez — me dit-il d'un ton terrible — fuyez, hâtez-vous de
fuir! Ici la terre ne porte pour fruit que du poison: l'air
qu'on respire est empesté; les hommes contagieux ne se parlent
que pour se communiquer un venin mortel. La volupté lâche et
infâme, qui est le plus horrible des maux sortis de la boîte de
Pandore, amollit tous les coeurs et ne souffre ici aucune vertu.
Fuyez! Que tardez-vous? Ne regardez pas même derrière vous en
fuyant; effacez jusques au moindre souvenir de cette île
exécrable."
Il dit, et aussitôt je sentis comme un nuage épais qui se
dissipait sur mes yeux et qui me laissait voir la pure lumière:
une joie douce et pleine d'un ferme courage renaissait dans mon
coeur. Cette joie était bien différente de cette autre joie
molle et folâtre dont mes sens avaient été d'abord empoisonnés:
l'une est une joie d'ivresse et de trouble, qui est entrecoupée
de passions furieuses et de cuisants remords; l'autre est une
joie de raison, qui a quelque chose de bienheureux et de
céleste; elle est toujours pure et égale, rien ne peut
l'épuiser; plus on s'y plonge, plus elle est douce; elle ravit
l'âme sans la troubler. Alors je versai des larmes de joie, et
je trouvais que rien n'était si doux que de pleurer ainsi. — O
heureux — disais-je — les hommes à qui la vertu se montre dans
toute sa beauté! Peut-on la voir sans l'aimer? Peut-on l'aimer
sans être heureux?
Mentor me dit:
"Il faut que je vous quitte: je pars dans ce moment; il ne m'est
pas permis de m'arrêter."
"Où allez-vous donc? — lui répondis-je — en quelle terre
inhabitable ne vous suivrai-je point? Ne croyez pas pouvoir
m'échapper; je mourrai plutôt sur vos pas."
En disant ces paroles, je le tenais serré de toute ma force.
"C'est en vain — me dit-il — que vous espérez de me retenir. Le
cruel Métophis me vendit à des Ethiopiens ou Arabes. Ceux-ci,
étant allés à Damas, en Syrie, pour leur commerce, voulurent se
défaire de moi, croyant en tirer une grande somme d'un nommé
Hasaël, qui cherchait un esclave grec pour connaître les moeurs
de la Grèce et pour s'instruire de nos sciences. En effet,
Hasaël m'acheta chèrement. Ce que je lui ai appris de nos moeurs
lui a donné la curiosité de passer dans l'île de Crète pour
étudier les sages lois de Minos. Pendant notre navigation, les
vents nous ont contraints de relâcher dans l'île de Chypre. En
attendant un vent favorable, il est venu faire ses offrandes au
temple: le voilà qui en sort; les vents nous appellent; déjà nos
voiles s'enflent. Adieu, cher Télémaque; un esclave qui craint
les dieux doit suivre fidèlement son maître. Les dieux ne me
permettent plus d'être à moi: si j'étais à moi, ils le savent,
je ne serais qu'à vous seul. Adieu, souvenez-vous des travaux
d'Ulysse et des larmes de Pénélope; souvenez-vous des justes
dieux. O dieux, protecteurs de l'innocence, en quelle terre
suis-je contraint de laisser Télémaque!"
"Non, non — lui dis-je — mon cher Mentor, il ne dépendra pas de
vous de me laisser ici: plutôt mourir que de vous voir partir
sans moi. Ce maître syrien est-il impitoyable? Est-ce une
tigresse dont il a sucé les mamelles dans son enfance?
Voudra-t-il vous arracher d'entre mes bras? Il faut qu'il me
donne la mort ou qu'il souffre que je vous suive. Vous
m'exhortez vous-même à fuir et vous ne voulez pas que je fuie en
suivant vos pas! Je vais parler à Hasaël; il aura peut-être
pitié de ma jeunesse et de mes larmes: puisqu'il aime la sagesse
et qu'il va si loin la chercher, il ne peut point avoir un coeur
féroce et insensible. Je me jetterai à ses pieds, j'embrasserai
ses genoux, je ne le laisserai point aller qu'il ne m'ait
accordé de vous suivre. Mon cher Mentor, je me ferai esclave
avec vous; je lui offrirai de me donner à lui: s'il me refuse,
c'est fait de moi, je me délivrerai de la vie."
Dans ce moment Hasaël appela Mentor; je me prosternai devant
lui. Il fut surpris de voir un inconnu en cette posture.
"Que voulez-vous?" me dit-il.
"La vie, répondis-je; car je ne puis vivre, si vous ne souffrez
que je suive Mentor, qui est à vous. Je suis le fils du grand
Ulysse, le plus sage des rois de la Grèce qui ont renversé la
superbe ville de Troie, fameuse dans toute l'Asie. Je ne vous
dis point ma naissance pour me vanter, mais seulement pour vous
inspirer quelque pitié de mes malheurs. J'ai cherché mon père
par toutes les mers, ayant avec moi cet homme, qui était pour
moi un autre père. La fortune, pour comble de maux, me l'a
enlevé; elle l'a fait votre esclave: souffrez que je le sois
aussi. S'il est vrai que vous aimiez la justice et que vous
alliez en Crète pour apprendre les lois du bon roi Minos,
n'endurcissez point votre coeur contre mes soupirs et contre mes
larmes. Vous voyez le fils d'un roi, qui est réduit à demander
la servitude comme son unique ressource. Autrefois j'ai voulu
mourir en Sicile pour éviter l'esclavage, mais mes premiers
malheurs n'étaient que de faibles essais des outrages de la
fortune: maintenant je crains de ne pouvoir être reçu parmi vos
esclaves. O dieux, voyez mes maux; ô Hasaël, souvenez-vous de
Minos, dont vous admirez la sagesse et qui nous jugera tous deux
dans le royaume de Pluton." Hasaël, me regardant avec un visage
doux et humain, me tendit la main, et me releva:
"Je n'ignore pas, me dit-il, la sagesse et la vertu d'Ulysse;
Mentor m'a raconté souvent quelle gloire il a acquise parmi les
Grecs; et d'ailleurs la prompte renommée a fait entendre son nom
à tous les peuples de l'Orient. Suivez-moi, fils d'Ulysse; je
serai votre père, jusqu'à ce que vous ayez retrouvé celui qui
vous a donné la vie. Quand même je ne serais pas touché de la
gloire de votre père, de ses malheurs et des vôtres, l'amitié
que j'ai pour Mentor m'engagerait à prendre soin de vous. Il est
vrai que je l'ai acheté comme esclave, mais je le garde comme un
ami fidèle; l'argent qu'il m'a coûté m'a acquis le plus cher et
le plus précieux ami que j'aie sur la terre. J'ai trouvé en lui
la sagesse; je lui dois tout ce que j'ai d'amour pour la vertu.
Dès ce moment, il est libre; vous le serez aussi: je ne vous
demande, à l'un et à l'autre, que votre coeur."
En un instant je passai de la plus amère douleur à la plus vive
joie que les mortels puissent sentir. Je me voyais sauvé d'un
horrible danger; je m'approchais de mon pays; je trouvais un
secours pour y retourner; je goûtais la consolation d'être
auprès d'un homme qui m'aimait déjà par le pur amour de la
vertu; enfin je me retrouvais tout en retrouvant Mentor pour ne
le plus quitter.
Hasaël s'avance sur le sable du rivage: nous le suivons; on
entre dans le vaisseau; les rameurs fendent les ondes paisibles;
un zéphyr léger se joue de nos voiles, il anime tout le vaisseau
et lui donne un doux mouvement. L'île de Chypre disparaît
bientôt. Hasaël, qui avait impatience de connaître mes
sentiments, me demanda ce que je pensais des moeurs de cette
île. Je lui dis ingénument en quel danger ma jeunesse avait été
exposée et le combat que j'avais souffert au-dedans de moi. Il
fut touché de mon horreur pour le vice et dit ces paroles: "O
Vénus, je reconnais votre puissance et celle de votre fils; j'ai
brûlé de l'encens sur vos autels; mais souffrez que je déteste
l'infâme mollesse des habitants de votre île et l'impudence
brutale avec laquelle ils célèbrent vos fêtes."
Ensuite il s'entretenait avec Mentor de cette première puissance
qui a formé le ciel et la terre, de cette lumière simple,
infinie et immuable, qui se donne à tous sans se partager; de
cette vérité souveraine et universelle qui éclaire tous les
esprits, comme le soleil éclaire tous les corps. "Celui -
ajoutait-il — qui n'a jamais vu cette lumière pure est aveugle
comme un aveugle-né; il passe sa vie dans une profonde nuit,
comme les peuples que le soleil n'éclaire point pendant
plusieurs mois de l'année; il croit être sage, et il est
insensé; il croit tout voir, et il ne voit rien; il meurt
n'ayant jamais rien vu; tout au plus il aperçoit de sombres et
fausses lueurs, de vaines ombres, des fantômes qui n'ont rien de
réel. Ainsi sont tous les hommes entraînés par le plaisir des
sens et par le charme de l'imagination. Il n'y a point sur la
terre de véritables hommes, excepté ceux qui consultent, qui
aiment, qui suivent cette raison éternelle: c'est elle qui nous
inspire quand nous pensons bien, c'est elle qui nous reprend
quand nous pensons mal. Nous ne tenons pas moins d'elle la
raison que la vie. Elle est comme un grand océan de lumière: nos
esprits sont comme de petits ruisseaux qui en sortent et qui y
retournent pour s'y perdre."
Quoique je ne comprisse point encore parfaitement la profonde
sagesse de ces discours, je ne laissais pas d'y goûter je ne
sais quoi de pur et de sublime; mon coeur en était échauffé et
la vérité me semblait reluire dans toutes ces paroles. Ils
continuèrent à parler de l'origine des dieux, des héros, des
poètes, de l'âge d'or, du déluge, des premières histoires du
genre humain, du fleuve d'oubli où se plongent les âmes des
morts, des peines éternelles préparées aux impies dans le
gouffre noir du Tartare, et de cette heureuse paix dont
jouissent les justes dans les Champs Elysées, sans crainte de
pouvoir la perdre.
Pendant qu'Hasaël et Mentor parlaient, nous aperçûmes des
dauphins couverts d'une écaille qui paraissait d'or et d'azur.
En se jouant, ils soulevaient les flots avec beaucoup d'écume.
Après eux venaient des Tritons, qui sonnaient de la trompette
avec leurs conques recourbées. Ils environnaient le char
d'Amphitrite, traîné par des chevaux marins, plus blancs que la
neige, et qui, fendant l'onde salée, laissaient loin derrière
eux un vaste sillon dans la mer. Leurs yeux étaient enflammés et
leurs bouches étaient fumantes. Le char de la déesse était une
conque d'une merveilleuse figure; elle était d'une blancheur
plus éclatante que l'ivoire, et les roues étaient d'or. Ce char
semblait voler sur la face des eaux paisibles. Une troupe de
nymphes couronnées de fleurs nageaient en foule derrière le
char; leurs beaux cheveux pendaient sur leurs épaules et
flottaient au gré du vent. La déesse tenait d'une main un
sceptre d'or pour commander aux vagues, de l'autre elle portait
sur ses genoux le petit dieu Palémon, son fils, pendant à sa
mamelle. Elle avait un visage serein et une douce majesté qui
faisait fuir les vents séditieux et toutes les noires tempêtes.
Les Tritons conduisaient les chevaux et tenaient les rênes
dorées. Une grande voile de pourpre flottait dans l'air
au-dessus du char; elle était à demi enflée par le soufre d'une
multitude de petits Zéphyrs qui s'efforçaient de la pousser par
leurs haleines. On voyait au milieu des airs Eole empressé,
inquiet et ardent. Son visage ridé et chagrin, sa voix
menaçante, ses sourcils épais et pendants, ses yeux d'un feu
sombre et austère tenaient en silence les fiers aquilons et
repoussaient tous les nuages. Les immenses baleines et tous les
monstres marins, faisant avec leurs narines un flux et reflux de
l'onde amère, sortaient à la hâte des grottes profondes, pour
voir la déesse.

 
 
 

<<< sommaire  <<< page précédente  page suivante >>>
